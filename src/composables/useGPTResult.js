// useGPTResult.js
import {OpenAIClient, AzureKeyCredential} from "@azure/openai";
import {ref} from "vue";

export default function useGPTResult(...prompts) {
    const results = ref([]);

    const endpoint = 'https://axo-openai-nuitinfo.openai.azure.com/';
    const azureApiKey = '5e23478df3ef42eb9abe7000d0825310';

    async function getResults() {
        const client = new OpenAIClient(endpoint, new AzureKeyCredential(azureApiKey));
        const deploymentId = "p9e7y3677m6fng32852frd7m8fwecy59";

        for (const prompt of prompts) {
            const message = {role: "user", content: prompt};
            const result = await client.getChatCompletions(deploymentId, [message]);

            if (result.choices.length > 0) {
                const choice = result.choices[0].message;
                results.value.push({prompt, result: choice});
            } else {
                console.warn(`No response received for prompt: ${prompt}`);
                results.value.push({prompt, result: null});
            }
        }

        return results.value;
    }

    function callGPT() {
        return getResults().catch((err) => {
            console.error("The sample encountered an error:", err);
        });
    }


    console.log(results.value);


    return {results, callGPT};
}
