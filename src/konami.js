let konamiSequence = ["ArrowUp", "ArrowUp", "ArrowDown", "ArrowDown", "ArrowLeft", "ArrowRight", "ArrowLeft", "ArrowRight", "b", "a", "Enter"];
let sequenceIndex = 0;
let konamiActivated = false;

let audio = document.getElementById("konamiSound")
audio.src = "./src/assets/specialist_loop.mp3"
audio.loop = true;
audio.load();

let isaacGif = document.getElementById("isaac");

document.addEventListener("keydown", (keyboardEvent) => {
    console.log(keyboardEvent);
    if (keyboardEvent.key.toLowerCase() === konamiSequence[sequenceIndex].toLowerCase()) {
        sequenceIndex++;
        if (sequenceIndex === konamiSequence.length) {
            konamiActivated = true;
            sequenceIndex = 0;
            console.log("KONAMI CODE ACTIVATED");
        }
    } else if (keyboardEvent.key === "Escape") {
        konamiActivated = false;
    } else {
        sequenceIndex = 0;
    }

    if (konamiActivated) {
        audio.play();
        isaacGif.hidden = false;
    } else {
        isaacGif.hidden = true;
        audio.pause();
        audio.duration = 0;
        audio.load();
    }
});


